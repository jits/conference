package com.conference.core.dao;

import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class ReaderTest extends TestCase {
	
	private ReceiverDAO reader;
	
	@Before
	public void setUp()  throws Exception {
		reader = new ReceiverFileImpl();
	}

	@Test
	public void testPopulate() {
		List<String> lines = reader.receive();
		assertFalse(lines.isEmpty());
		
		for (Iterator<String> iterator = lines.iterator(); iterator.hasNext();) {
			String talk = (String) iterator.next();
			assertNotNull(talk);
			assertTrue(talk.length() > 0);
		}
		
	}

	@After
	public void tearDown() throws Exception  {
		reader = null;
	}
}
