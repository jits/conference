package com.conference.core.dao;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ReceiverFileImpl implements ReceiverDAO {

	public List<String> receive() {
		
		List<String> talks = new LinkedList<>();
		
		try {
			InputStream inputStream = ReceiverFileImpl.class.getClassLoader().getResourceAsStream("input.txt");		
			Scanner scan = new Scanner(inputStream, "UTF-8");
	     	while ( scan.hasNext()) {
				String line = scan.nextLine();
				talks.add(line);
			}
			scan.close();
     	} catch (Exception e) {
     		System.out.println("Erro to recever file: " + e);
		}
		
		return talks;
	}
}
