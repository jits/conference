package com.conference.core.dao;

import java.util.List;

public interface ReceiverDAO {
	
	public List<String> receive();

}
