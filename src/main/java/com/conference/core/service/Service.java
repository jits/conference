package com.conference.core.service;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.conference.core.constant.Period;
import com.conference.core.model.Networking;
import com.conference.core.model.Session;
import com.conference.core.model.Talk;
import com.conference.core.model.Track;

public class Service {
	
	public static void register(Period period, int start, int finish, List<Talk> talks, List<Talk> talksAdded,
			Calendar schedule, Track track) {
		
		Session session = Session.create(period, start, finish);
		schedule.set(Calendar.HOUR_OF_DAY, session.getBegin());
		
		for (Iterator<Talk> iTalk = talks.iterator(); iTalk.hasNext();) {
			Talk talk = (Talk) iTalk.next();
			if (talk.getLength() == 0) {
				talk.setLength(Talk.DEFAULT);
			}
			Calendar lunch = (Calendar) schedule.clone();
			lunch.add(Calendar.MINUTE, talk.getLength());
			if (!session.getTalks().containsKey(schedule.getTimeInMillis())
					&& schedule.get(Calendar.HOUR_OF_DAY) < session.getFinish()
					&& lunch.get(Calendar.HOUR_OF_DAY) < session.getFinish()) {
				talk.setDate(schedule.getTime());
				session.getTalks().put(schedule.getTimeInMillis(), talk);
				schedule.add(Calendar.MINUTE, talk.getLength());
				talksAdded.add(talk);
			}
		}
		if ( period == Period.MORNING && 
			 schedule.get(Calendar.HOUR_OF_DAY) <= session.getFinish()) {			
			Talk talk = Talk.create(Period.Lunch.name());
			talk.setLength(Track.LUNCH);
			schedule.set(Calendar.HOUR_OF_DAY, 12);
			schedule.set(Calendar.MINUTE, 0);
			talk.setDate(schedule.getTime());
			session.getTalks().put(schedule.getTimeInMillis(), talk);
		} else {
			Networking networking = new Networking();
			networking.setSchedule(schedule.getTime());
			if ( schedule.get(Calendar.HOUR_OF_DAY) >= finish) {
				Calendar event = Calendar.getInstance();
				event.setTime(networking.getSchedule());
				event.add(Calendar.MINUTE, -10);			
				networking.setSchedule(event.getTime());
			} 
			session.setNetworking(networking);
		}
		
		track.getSessions().add(session);
		for (Iterator<Talk> iterator = talksAdded.iterator(); iterator.hasNext();) {
			Talk talk = (Talk) iterator.next();
			talks.remove(talk);
		}
	}
	

}
