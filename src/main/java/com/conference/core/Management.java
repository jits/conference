package com.conference.core;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.conference.core.constant.Period;
import com.conference.core.dao.ReceiverDAO;
import com.conference.core.dao.ReceiverFileImpl;
import com.conference.core.model.Conference;
import com.conference.core.model.Networking;
import com.conference.core.model.Session;
import com.conference.core.model.Talk;
import com.conference.core.model.Track;
import com.conference.core.service.Service;

public class Management {

	public static void main(String[] args) throws FileNotFoundException {

		Conference conference = new Conference();

		List<Talk> talks = new LinkedList<>();
		List<Talk> talksAdded = new LinkedList<>();

		ReceiverDAO receiver = new ReceiverFileImpl();
		List<String> talkList = receiver.receive();
		for (Iterator<String> iterator = talkList.iterator(); iterator.hasNext();) {
			String line = (String) iterator.next();
			Talk talk = Talk.create(line);
			talks.add(talk);
		}
		
		Calendar dayOne = Calendar.getInstance();
		Track track = Track.create(1, dayOne);

		Service.register(Period.MORNING, track.getBegin(), 12, talks, talksAdded, dayOne, track);
		dayOne.add(Calendar.MINUTE, Track.LUNCH);
		Service.register(Period.AFTERNOON, 13, track.getFinish(), talks, talksAdded, dayOne, track);

		conference.getTracks().add(track);		

		Calendar dayTwo = Calendar.getInstance();
		track = Track.create(2, dayTwo);

		Service.register(Period.MORNING, track.getBegin(), 12, talks, talksAdded, dayTwo, track);
		dayTwo.add(Calendar.MINUTE, Track.LUNCH);
		Service.register(Period.AFTERNOON, 13, track.getFinish(), talks, talksAdded, dayTwo, track);
		
		conference.getTracks().add(track);

		SimpleDateFormat pattern = new SimpleDateFormat("hh:mma");
		showConference(pattern, conference);

	}

	private static void showConference(SimpleDateFormat pattern, Conference conference) {
		List<Track> tracks = conference.getTracks();
		for (Iterator<Track> iTrack = tracks.iterator(); iTrack.hasNext();) {
			Track track = (Track) iTrack.next();
			printTrack(track);
			List<Session> sessions = track.getSessions();
			showSessions(pattern, sessions);
		}
	}

	private static void showSessions(SimpleDateFormat pattern, List<Session> sessions) {
		Session session;
		for (Iterator<Session> iSession = sessions.iterator(); iSession.hasNext();) {
			session = (Session) iSession.next();
			Collection<Talk> talkies = session.getTalks().values();
			for (Iterator<Talk> iTalk = talkies.iterator(); iTalk.hasNext();) {
				Talk talk = (Talk) iTalk.next();
				StringBuffer sb = new StringBuffer();
				sb.append(pattern.format(talk.getDate()))
				  .append(" ").append(talk.getTitle());
				if ( !talk.getTitle().startsWith(Period.Lunch.name())) {
				    sb.append(talk.getLength()).append("min");
				}
				System.out.println(sb.toString());
			}
			Networking networking = session.getNetworking();
			if ( networking != null) {
				StringBuffer sb = new StringBuffer();
				sb.append(pattern.format(networking.getSchedule()))
				  .append(" ").append(networking.getName());
				System.out.println(sb.toString());
			}
		}
	}

	private static void printTrack(Track track) {
		StringBuffer sb = new StringBuffer();
		sb.append(track.getName()).append(" ").append(track.getOrder()).append(":");
		System.out.println(sb.toString());
	}

}
