package com.conference.core.model;

import java.util.Date;

public class Talk {
	
	private static final int END = 3;
	private static final int BEGIN = 5;
	public static final int DEFAULT = 30;
	
	private int length;
	private String title;
	private Date date;
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public static Talk create(String line) {
		Talk talk = new Talk();
	    if ( line.endsWith("min") ) {
	    	int beginIndex = line.length() - BEGIN;
			int endIndex = line.length() - END;
			String length = line.substring(beginIndex, endIndex);
	    	talk.setLength(Integer.valueOf(length));
	    	talk.setTitle(line.substring(0, beginIndex));
	    } else {
	    	talk.setLength(30);
	    	talk.setTitle(line+ " ");
	    }

	    return talk;
	}
}
