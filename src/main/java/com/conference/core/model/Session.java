package com.conference.core.model;

import java.util.LinkedHashMap;
import java.util.Map;

import com.conference.core.constant.Period;

public class Session {
	
	private int begin;
	private int finish;
	private Period period;
	private Networking networking;

	public int getBegin() {
		return begin;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public int getFinish() {
		return finish;
	}

	public void setFinish(int finish) {
		this.finish = finish;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	Map<Long, Talk> talks = new LinkedHashMap<>();

	public Map<Long, Talk> getTalks() {
		return talks;
	}

	public void setTalks(Map<Long, Talk> presenters) {
		this.talks = presenters;
	}

	public Networking getNetworking() {
		return networking;
	}

	public void setNetworking(Networking networking) {
		this.networking = networking;
	}	
	
	public static Session create(Period period, int start, int finish) {
		Session session = new Session();
		session.setPeriod(period);
		session.setBegin(start);
		session.setFinish(finish);
		return session;
	}
	
	
}
