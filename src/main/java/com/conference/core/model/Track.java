package com.conference.core.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Track {
	
	public static int LUNCH = 60;
	
	private int order;
	private String name = "Track";
	private int begin;
	private int finish;
	private Date schedule;
	
	public Track(int order, int begin, int finish, Date schedule) {
		this.order = order;
		this.begin = begin;
		this.finish = finish;
		this.schedule = schedule;
	}
	
	List<Session> sessions = new ArrayList<Session>();
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBegin() {
		return begin;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public int getFinish() {
		return finish;
	}

	public void setFinish(int finish) {
		this.finish = finish;
	}

	public Date getDate() {
		return schedule;
	}

	public void setDate(Date schedule) {
		this.schedule = schedule;
	}

	public List<Session> getSessions() {
		return sessions;
	}

	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}
	
	public static Track create(int order, Calendar scheduke) {
		Track track = new Track(order, 9, 17, scheduke.getTime());
		scheduke.setTime(new Date());
		scheduke.set(Calendar.HOUR_OF_DAY, 9);
		scheduke.set(Calendar.MINUTE, 0);
		scheduke.set(Calendar.SECOND, 0);
		return track;
	}
}
