package com.conference.core.model;

import java.util.LinkedList;
import java.util.List;

public class Conference {

	List<Track> tracks = new LinkedList<>();

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	
}
