package com.conference.core.model;

import java.util.Date;

public class Networking {
	
	private String name = "Networking Event";
	private Date schedule;
	
	public String getName() {
		return name;
	}

	public Date getSchedule() {
		return schedule;
	}
	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}
}